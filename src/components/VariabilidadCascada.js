import vSelect from 'vue-select'
import axios from 'axios'

export default {
  name: 'variabilidad-cascada',
  components: {
    vSelect
  },
  data () {
    return {
      comunas: [],
      data: [],
      chart: null,
      selectedComuna: ''
    }
  },
  mounted () {
    this.loadComunas()
    .then(this.loadSeries)
    .then(this.loadGraph)
  },
  computed: {
    chartNames () {
      return this.comunas.map(x => x.Comuna)
    },
    series () {
      const series = []
      for (const k in this.data) { series.push(k) }
      return series
    }
  },
  methods: {
    createChart () {
      this.chart = AmCharts.makeChart('grafico-cascada-de-variabilidad', {
        'type': 'serial',
        'categoryField': 'category',
        'rotate': true,
        'marginBottom': 10,
        'sequencedAnimation': false,
        'startDuration': 0,
        'theme': 'light',
        'categoryAxis': {
          'gridPosition': 'start'
        },
        'trendLines': [],
        'graphs': this.generateChartGraphs(),
        'guides': [],
        'valueAxes': [
          {
            'id': 'ValueAxis-1',
            'zeroGridAlpha': -1,
            'gridThickness': 0,
            'position': 'top'
          }
        ],
        'chartCursor': {
          'valueLineEnabled': true,
          'valueLineBalloonEnabled': true,
          'fullWidth': false,
          'oneBalloonOnly': true
        },
        'allLabels': [],
        'balloon': {},
        'titles': [],
        'dataProvider': this.generateChartDataProvider()
      })
    },
    loadGraph () {
      if (AmCharts.isReady) {
        this.createChart()
      } else {
        AmCharts.ready(() => {
          this.createChart()
        })
      }
    },
    generateChartGraphs () {
      const graphs = []
      this.comunas.forEach(c => {
        graphs.push({
          'balloonText': c.Comuna + ' [[value]] el [[category]]',
          'bullet': 'round',
          'bulletBorderThickness': 0,
          'bulletHitAreaSize': 0,
          'columnWidth': 0,
          'connect': false,
          'cornerRadiusTop': 64,
          'fixedColumnWidth': -4,
          'fontSize': -5,
          'gapPeriod': 1,
          'id': 'graph-' + c.Comuna,
          'periodSpan': 80,
          'switchable': false,
          'title': c.Comuna,
          'topRadius': 0,
          'lineColor': '#ccc',
          'type': 'smoothedLine',
          'valueField': c.Comuna,
          'visibleInLegend': false
        })
      })
      return graphs
    },
    generateChartDataProvider () {
      const provider = []
      for (const graphName in this.data) {
        const graph = this.data[graphName]
        graph['category'] = graphName
        provider.push(graph)
      }
      return provider
    },
    highlightComuna (comuna, color, thikness) {
      const chart = this.chart
      const index = chart.graphs.filter(g => g.title === comuna)[0].index
      const graph = chart.graphs[index]
      const lastGraph = chart.graphs.length - 1
      const oldIndex = graph.index
      const oldZeroGraph = chart.graphs[lastGraph]
      graph.index = lastGraph
      graph.lineColor = color
      graph.lineThickness = thikness
      oldZeroGraph.index = oldIndex
      chart.graphs = chart.graphs.sort((a, b) => {
        if (a.index === b.index) {
          return 0
        }
        return a.index < b.index ? -1 : 1
      })
      chart.validateNow()
    },
    unHighlightComuna () {
      const chart = this.chart
      const index = chart.graphs.length - 1
      const graph = chart.graphs[index]

      graph.lineColor = '#CCCCCC'
      graph.lineThickness = 1
    },
    loadComunas () {
      return axios
        .get('static/data/comunas_regiones_codes.json')
        .then(results => results.data)
        .then(comunas => {
          this.comunas = comunas
        })
    },
    loadSeries () {
      return axios
        .get('static/data/cambio_por_comuna.json')
        .then(results => results.data)
        .then(data => {
          this.data = data
        })
    },
    selectedComunaChanged (comunaDatum) {
      const comuna = comunaDatum.Comuna
      if (this.selectedComuna) {
        this.unHighlightComuna()
      }
      this.selectedComuna = comuna
      this.highlightComuna(comuna, '#FF0000', 4)
      this.chart.validateNow()
    }
  }

}
